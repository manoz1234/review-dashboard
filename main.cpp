#include <iostream>
#include <curl/curl.h>
#include <vector>
#include <string>

// Struct to hold comment data
struct Comment {
   std::string author;
   std::string body;
};

   // Callback function for curl to write data to a string
   size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata) {
   std::string* response = reinterpret_cast<std::string*>(userdata);
   response->append(ptr, size * nmemb);
   return size * nmemb;
}

int main() {
   // Variables to hold project ID and URL
   int project_id = 45260506 ;  // Replace with your project ID
   std::string project_url = "https://gitlab.com/manoz1234/review-dashboard/activity" + std::to_string(project_id);

   // Initialize curl
   curl_global_init(CURL_GLOBAL_ALL);
   CURL* curl = curl_easy_init();

   // Set curl options
   curl_easy_setopt(curl, CURLOPT_URL, project_url.c_str());
   curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);

   // Disable SSL verification (not recommended in production)
   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

   //Set headers for API call
   struct curl_slist* headers = NULL;
   headers = curl_slist_append(headers, "Private-Token: <your_private_token>");
   headers = curl_slist_append(headers, "Content-Type: application/json");
   curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

   // Make API call to retrieve project name
   std::string response;
   curl_easy_perform(curl);
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

   // Extract project name from response
   std::string project_name;
   size_t start_pos = response.find("\"name\":\"") + 8;
   size_t end_pos = response.find("\"", start_pos);
   project_name = response.substr(start_pos, end_pos - start_pos);

   // Print project name
   std::cout << "Project Name: " << project_name << std::endl;

   // Make API call to retrieve comments
   std::string comments_url = project_url + "/issues/123/comments";  // Replace issue number with the one you want to retrieve
   curl_easy_setopt(curl, CURLOPT_URL, comments_url.c_str());
   response.clear();
   curl_easy_perform(curl);
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

   // Parse JSON response to extract comment data
   std::vector<Comment> comments;
   size_t comments_start_pos = response.find("[{");
   size_t comments_end_pos = response.rfind("}]") + 2;
   std::string comments_str = response.substr(comments_start_pos, comments_end_pos - comments_start_pos);
   size_t comment_start_pos = comments_str.find("{\"author\":{\"name\":\"") + 20;

   while (comment_start_pos != std::string::npos) {
          Comment comment;

     // Extract author name
    size_t comment_author_end_pos = comments_str.find("\"", comment_start_pos);
    comment.author = comments_str.substr(comment_start_pos, comment_author_end_pos - comment_start_pos);
     // Extract comment body
     size_t comment_body_start_pos = comments_str.find(",\"body\":\"",comment_author_end_pos) + 9;
     size_t comment_body_end_pos = comments_str.find("\"", comment_body_start_pos);
     comment.body = comments_str.substr(comment_body_start_pos, comment_body_end_pos - comment_body_start_pos);

  // comments_str.find("\"", comment_body_start_pos);
  // comment.body = comments_str.substr(comment_body_start_pos, comment_body_end_pos - comment_body_start_pos);

   // Add comment to vector
   comments.push_back(comment);

   // Find next comment start position
   comment_start_pos = comments_str.find("{\"author\":{\"name\":\"", comment_body_end_pos) + 20;
}

   // Print comments
   std::cout << "Comments:" << std::endl;
   for (const auto& comment : comments) {
   std::cout << "Author: " << comment.author << std::endl;
   std::cout << "Comment: " << comment.body << std::endl;
   std::cout << std::endl;
}

   // Clean up
   curl_slist_free_all(headers);
   curl_easy_cleanup(curl);
   curl_global_cleanup();
   return 0;
   }
