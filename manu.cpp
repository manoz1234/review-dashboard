#include <curl/curl.h>
#include <iostream>

// GitLab API endpoint and authorization token
const std::string api_url = "https://gitlab.example.com/api/v4";
const std::string token = "YOUR_ACCESS_TOKEN";

// Merge request ID to retrieve data for
const int merge_request_id = 1234;

// Callback function for libcurl to write response data to a string
size_t write_callback(char* ptr, size_t size, size_t nmemb, std::string* data)
{
   size_t real_size = size * nmemb;
   data->append(ptr, real_size);
   return real_size;
}

int main()
{
   // Initialize libcurl
   curl_global_init(CURL_GLOBAL_DEFAULT);
   CURL* curl_handle = curl_easy_init();

   if (curl_handle)
   {
       // Set up curl options
       std::string api_endpoint = api_url + "/merge_requests/" + std::to_string(merge_request_id);
       curl_easy_setopt(curl_handle, CURLOPT_URL, api_endpoint.c_str());
       curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, "PRIVATE-TOKEN: " + token);
       curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_callback);

       // Disable SSH verification
       curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, false);
       curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYHOST, false);

       // Send API request and retrieve response
       std::string response_data;
       curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &response_data);
       CURLcode result = curl_easy_perform(curl_handle);

       if (result == CURLE_OK)
       {
           // Parse JSON response
           // Note: you will need to install a JSON parsing library for C++
           // such as nlohmann/json or RapidJSON to parse the JSON data.
           // Here's an example using nlohmann/json:
           nlohmann::json data = nlohmann::json::parse(response_data);

           // Extract developer name, reviewer name, reviewer comment, and file names
           std::string developer_name = data["author"]["name"];
           std::string reviewer_name = data["assignee"]["name"];
           int reviewer_comment_count = data["user_notes_count"];
           std::vector<std::string> file_names;
           for (auto& change : data["changes"])
           {
               file_names.push_back(change["new_path"]);
           }

           // Print extracted data
           std::cout << "Developer Name: " << developer_name << std::endl;
           std::cout << "Reviewer Name: " << reviewer_name << std::endl;
           std::cout << "Reviewer Comment Count: " << reviewer_comment_count << std::endl;
           std::cout << "File Names: ";
           for (const auto& file_name : file_names)
           {
               std::cout << file_name << " ";
           }
           std::cout << std::endl;
       }
       else
       {
           std::cerr << "Error retrieving data from GitLab API: " << curl_easy_strerror(result) << std::endl;
       }

       // Clean up
       curl_easy_cleanup(curl_handle);
   }

   curl_global_cleanup();
   return 0;
}
