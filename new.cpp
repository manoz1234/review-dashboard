#include <iostream>
#include <curl/curl.h>

int main() {
   // Disable SSL verification
   curl_global_init(CURL_GLOBAL_DEFAULT);
   CURL *curl = curl_easy_init();
   if (curl) {
   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);

   // Set GitLab API endpoint and authentication token
   std::string url = "https://gitlab.com/manoz1234/review-dashboard";
   std::string token = "glpat-xixo-Hnia2DNBBHn8yZJ";
   curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
   curl_slist *headers = nullptr;
   headers = curl_slist_append(headers, ("glpat-xixo-Hnia2DNBBHn8yZJ: " + token).c_str());
   curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

   // Perform the HTTP request
   CURLcode res;
   std::string response;
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, [](char* ptr, size_t size, size_t nmemb, void* userdata) -> size_t {
   std::string* str = reinterpret_cast<std::string*>(userdata);
   str->append(ptr, size * nmemb);
   return size * nmemb;
   });
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
   res = curl_easy_perform(curl);
   curl_easy_cleanup(curl);

   // Parse the response and print project name
   std::size_t pos = response.find("\"name\":");
   if (pos != std::string::npos) {
   pos += 8; // Length of "\"name\":"
   std::size_t endpos = response.find("\"", pos);
   if (endpos != std::string::npos) {
   std::string project_name = response.substr(pos, endpos - pos);
   std::cout << "Project name: " << project_name << std::endl;
   }
   }
   }
   curl_global_cleanup();
   return 0;
}
