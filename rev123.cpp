#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <curl/curl.h>
#include <json/json.h>
#include <ctime>
#include <chrono>
#include<regex>
#include<map>

using namespace std;
using namespace std::chrono;
size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata)
{
    ((string*)userdata)->append(ptr, size * nmemb);
    return size * nmemb;
}

bool validateDate(const string& dateStr, bool checkFuture = true)
{
if(dateStr.length()!=10)
{
cout<<endl<< "please enter the date in the correct format"<<endl;
return false;
}
try
{
stringstream ss(dateStr);
tm date = {};
ss >> get_time(&date, "%d-%m-%Y");
if (ss.fail() || !ss.eof())
{
// Date validation failed
return false;
}
// Check if the entered date is within a valid range (e.g., not in the future)
auto enteredTime = system_clock::from_time_t(mktime(&date));
auto currentTime = system_clock::now();
if(checkFuture)
{
return (enteredTime <= currentTime);
}
else
{
	return(enteredTime < currentTime);
}
}
catch (const exception&)
{
// Exception occurred during date validation
return false;
}
}
bool checkDateTimeline(string date1,string date2)
{
    int day1 = stoi(date1.substr(0, 2));
    int month1 = stoi(date1.substr(3, 2));
    int year1 = stoi(date1.substr(6, 4));

    int day2 = stoi(date2.substr(0, 2));
    int month2 = stoi(date2.substr(3, 2));
    int year2 = stoi(date2.substr(6, 4));

    // Compare the two dates
    if (year2 < year1 || (year2 == year1 && month2 < month1) || (year2 == year1 && month2 == month1 && day2 < day1)) {
        cerr << "Second date cannot be earlier than first date." << endl;
        return 1;
    }

   // cout << "The second date is after or equal to the first date." << endl;

    return 0;

}
bool validateDayOfMonth(int day, int month, int year)
{
if (month < 1 || month > 12)
{
return false; // Invalid month
}
static const int daysInMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
int maxDay = daysInMonth[month - 1];
if (month == 2 && (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)))
{
maxDay = 29; // Leap year, so February has 29 days
}
if (to_string(year).length() != 4) {
cout << "Invalid input! Year must be in YYYY format.\n";
return false;
}
return (day >= 1 && day <= maxDay);
}


string getAccessTokenFromFile(const string& projectId) {
    string accessToken = "";
    string keyToFind = "project_id";
    string filename = "data.config";
    ifstream file(filename);
    if (file.is_open()) {
        string line;
        map<string, string> data;
        while (getline(file, line)) {
            // Splitting the line into key and value using delimiter "="
            size_t delimiterPos = line.find("=");
            if (delimiterPos != string::npos) {
                std::string key = line.substr(0, delimiterPos);
                std::string value = line.substr(delimiterPos + 1);
                // Trimming leading and trailing whitespaces from key and value
                key.erase(0, key.find_first_not_of(" \t"));
                key.erase(key.find_last_not_of(" \t") + 1);
                value.erase(0, value.find_first_not_of(" \t"));
                value.erase(value.find_last_not_of(" \t") + 1);
                data[key] = value;
            }
        }

        file.close();

        for (const auto& pair : data) {
            if (pair.second == projectId && pair.first.find(keyToFind) != std::string::npos) {
                std::string tokenKey = "access_token" + pair.first.substr(keyToFind.length());
                auto it = data.find(tokenKey);
                if (it != data.end()) {
                    accessToken = it->second;
                    break;
                }
            }
        }
    } else {
        std::cerr << "Unable to open the file." << std::endl;
    }

    return accessToken;
}

int main()
{
   // Initialize variables for user input
string date1_str, date2_str, projectId, gitlab_api_url, gitlab_access_token;
// Prompt user for start date
while(1)
{
while(1)
{
cout << "Enter start date (DD-MM-YYYY): ";
//cin >> date1_str;
getline(cin,date1_str);
if(date1_str.length()==0)
{
cout<<endl<<"Date cant be empty, please enter the date"<<endl;
continue;
}
// Validacgxute start date
if (!validateDate(date1_str, true))
{
cout << "Invalid start date or date is in the future" << endl;
continue;
}
// Parse start date components
int startDay, startMonth, startYear;
sscanf(date1_str.c_str(), "%d-%d-%d", &startDay, &startMonth, &startYear);
// Validate start day of month
if (!validateDayOfMonth(startDay, startMonth, startYear))
{
cout << "Invalid start date: Day is out of range for the specified month" << endl;
continue;
}else if (validateDayOfMonth(startDay, startMonth, startYear)==4)
{
cout << "Invalid input! Year must be in YYYY format.\n";
continue;
}
break;
}
// Prompt user for end date
while(1)
{
cout << "Enter end date (DD-MM-YYYY): ";
getline(cin,date2_str);   //cin >> date2_str;
if(date2_str.length()==0)
{
cout<<endl<<"Date cant be empty, please enter the date"<<endl;
continue;
}

// Validate end date
if (!validateDate(date2_str))
{
cout << " Invalid: You Entered Invalid End-Date" << endl;
continue;
}
//parse end date components
int endDay, endMonth, endYear;
sscanf(date2_str.c_str(), "%d-%d-%d", &endDay, &endMonth, &endYear);
// Validate end day of month
if (!validateDayOfMonth(endDay, endMonth, endYear))
{
cout << "Invalid: You Entered Invalid End-Date" << endl;
continue;
}else if (validateDayOfMonth(endDay, endMonth, endYear)==4)
{
cout << "Invalid input! Year must be in YYYY format.\n";
continue;
}
break;
}//inner while
if(checkDateTimeline(date1_str,date2_str))
{
cout<<"please re-enter the dates "<<endl;
continue;
}
break;
}//outer while
while(1){
cout << "Enter project ID: ";
//cin >> project_id;
getline(cin,projectId);
if(projectId.length()==0)
{
cout<<endl<<"Project id cant be empty, please enter the project id"<<endl;
continue;
}else if(projectId.length()>12)
{
cout<<endl<<"Please enter a valid project id"<<endl;
continue;
}
break;
}    
    gitlab_access_token =  getAccessTokenFromFile(projectId);
    

    // Construct GitLab API request URL for merge requests
   //gitlab_api_url = "https://gitlab.com/api/v4/projects/" + projectId + "/merge_requests";
    gitlab_api_url = "https://gitlab-gxp.cloud.health.ge.com/api/v4/projects/" + projectId + "/merge_requests?";
    cout<<gitlab_api_url<<endl;

    
    
    // CSV file path
    string csv_file_path = "comments.csv";

    // Set up libcurl
    CURL* curl;
    CURLcode res;
    curl = curl_easy_init();
    if (curl)
    {
        // Set the GitLab access token
        struct curl_slist *headerlist = NULL;
        string authorization = "Authorization: Bearer " + gitlab_access_token;
        headerlist = curl_slist_append(headerlist, authorization.c_str());
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);

        // Disable SSL verification
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

        // Open the CSV file for writing
        ofstream csv_file(csv_file_path);
        if (!csv_file.is_open())
        {
            cout << "Error: Could not open CSV file for writing" << endl;
            return 1;
        }

        // Write the CSV header
        csv_file << "Merge Request created date,Merge Request ID, Developer Name, Date, Reviewer Name, File Type,Line Number, Code Review Comments" << endl;

        // Variables for pagination
        int page = 1;
        bool more_pages = true;

        while (more_pages)
        {
            // Construct GitLab API request URL for merge requests with pagination
            string merge_requests_api_url = gitlab_api_url + "?page=" + to_string(page) + "&per_page=100";
            cout<<merge_requests_api_url<<endl;
            
            curl_easy_setopt(curl, CURLOPT_URL, merge_requests_api_url.c_str());

            // Set the callback function for the response
            string response;
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

            // Perform the request
            res = curl_easy_perform(curl);
            if (res == CURLE_OK)
            {
                // Parse the JSON data for merge requests
                Json::Value merge_requests_root;
                Json::Reader reader;
                bool merge_requests_parsing_successful = reader.parse(response, merge_requests_root);
                if (merge_requests_parsing_successful)
                {
                    // Check if there are more pages of merge requests
                    if (merge_requests_root.empty() || merge_requests_root.size() < 100)
                    {
                        more_pages = false;
                    }

                    // Loop through each merge request and get its comments
                    for (unsigned int i = 0; i < merge_requests_root.size(); i++)
                    {
                        string merge_request_created_at = merge_requests_root[i]["created_at"].asString();
                        string merge_request_id = merge_requests_root[i]["iid"].asString();

                        // Construct GitLab API request URL for comments on a merge request
                        //string comments_api_url = gitlab_api_url + "/" + merge_request_id + "/notes?created_after=" + date1_str + "T00:00:00.000Z&created_before=" + date2_str + "T23:59:59.999Z&per_page=100";                        
                        string comments_api_url = gitlab_api_url + "/" + merge_request_id + "/notes?created_after=" + date1_str + "T00:00:00.000Z&created_before=" + date2_str + "T23:59:59.999Z&per_page=100"; 
                        cout<<comments_api_url<<endl;

                        curl_easy_setopt(curl, CURLOPT_URL, comments_api_url.c_str());

                        // Perform the request
                        string comments_response;
						//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
                        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
                        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &comments_response);
                        res = curl_easy_perform(curl);

                        // Parse the JSON data for the comments on the merge request
                        Json::Value comments_root;
                        bool comments_parsing_successful = reader.parse(comments_response, comments_root);
                        if (comments_parsing_successful)
                        {
                            // Extract the relevant information from the JSON data and write to the CSV file
                            for (unsigned int j = 0; j < comments_root.size(); j++)
                            {
                                string assignee_name = merge_requests_root[i]["assignee"]["name"].asString();
                                string Date = comments_root[j]["created_at"].asString();
                                string reviewer_name = comments_root[j]["author"]["name"].asString();
                                string file_type = comments_root[j]["position"]["new_path"].asString();
                                int line_number = comments_root[j]["position"]["new_line"].asInt();
                                string comment_text = comments_root[j]["body"].asString();

                                // Replace any commas in the comment text with spaces
                                replace(comment_text.begin(), comment_text.end(), ',', ' ');
                                // Replace multiple consecutive spaces with a single
                                regex pattern("\\s+");
                                comment_text = regex_replace(comment_text, pattern, " ");
                                //Replace other special characters if needed  
                                // For example, replacing exclamation marks with periods                                                                                                               
                                replace(comment_text.begin(), comment_text.end(), '!', '.');

                                // Convert date1_str to a time_t object
                                struct tm date1_tm = {0};
                                istringstream date1_stream(date1_str);
                                date1_stream >> get_time(&date1_tm, "%d-%m-%Y");
                                time_t date1_time = mktime(&date1_tm);

                                // Convert date2_str to a time_t object
                                struct tm date2_tm = {0};
                                istringstream date2_stream(date2_str);
                                date2_stream >> get_time(&date2_tm, "%d-%m-%Y");
                                time_t date2_time = mktime(&date2_tm);

                                tm date_tm = {0, 0, 0, stoi(Date.substr(8, 2)), stoi(Date.substr(5, 2)) - 1, stoi(Date.substr(0, 4)) - 1900};
                                time_t c = mktime(&date_tm);

                                if (!file_type.empty() && (assignee_name != reviewer_name) && (difftime(c, date1_time) >= 0) && (difftime(date2_time, c) >= 0))
                                {
                                    csv_file << merge_request_created_at << " ,"  <<merge_request_id << ", " << assignee_name << ", " << Date << ", " << reviewer_name << ", " << file_type << ", " << line_number << " ," << comment_text << endl;
                                }
                            }
                        }
                        else
                        {
                            cout << "Error: Failed to parse JSON data for merge request comments" << endl;
                        }
                    }
                }
                else
                {
                    cout << "Error: Failed to parse JSON data for merge requests" << endl;
                }
            }
            else
            {
                cout << "Error: Failed to perform GitLab API request" << endl;
            }

            // Increment page number
            page++;
        }

        // Close the CSV file
        csv_file.close();
        cout << "CSV file with comments has been created successfully." << endl;

        // Clean up libcurl
        curl_easy_cleanup(curl);
    }
    else
    {
        cout << "Error: Failed to initialize libcurl" << endl;
        return 1;
    }

    return 0;
}
