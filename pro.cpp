#include <iostream>
#include <curl/curl.h>

using namespace std;

  // Callback function to handle response
  size_t handle_response(char *ptr, size_t size, size_t nmemb, void *userdata) {
   ((string*)userdata)->append(ptr, size * nmemb);
   return size * nmemb;
}

int main() {
   CURL *curl;
   CURLcode res;
   string readBuffer;
   string url = "https://gitlab.com/manoz1234/review-dashboard/activity/<45260506>/repository/commits?private_token=<glpat-GVSQzhSYevDPjv-JaBK1>&per_page=1";
   string auth_token = "Authorization: Bearer <glpat-GVSQzhSYevDPjv-JaBK1>";
   struct curl_slist *headers = NULL;

   // Disable SSH verification
   curl_global_init(CURL_GLOBAL_ALL);
   curl = curl_easy_init();
   if (curl) {
   curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false); // disable verification
   headers = curl_slist_append(headers, auth_token.c_str());
   headers = curl_slist_append(headers, "Content-Type: application/json");
   curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, handle_response);
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
   res = curl_easy_perform(curl);
   curl_slist_free_all(headers);
      curl_easy_cleanup(curl);
   }

   // Extract project name
   size_t project_start = readBuffer.find("title")+8;
   size_t project_end = readBuffer.find("\",\"created_at\"");
   string project_name = readBuffer.substr(project_start, project_end - project_start);
   cout << "Project Name: " << project_name << endl;

 // Extract comment body
   size_t comments_start = readBuffer.find("body")+7;
   size_t comments_end = readBuffer.find("\",\"created_at\"", comments_start);
   string comments = readBuffer.substr(comments_start, comments_end - comments_start);
   cout << "Comments: " << comments << endl;

   curl_global_cleanup();
   return 0;
}
