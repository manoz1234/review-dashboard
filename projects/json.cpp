#include <iostream>
#include <fstream>
//including JsonCpp header file
#include <jsoncpp/json/json.h>
using namespace std;
int main() {
//read file
ifstream file(“data.json”);
//json reader
Json::Reader reader;
//this will contain complete JSON data
Json::Value completeJsonData;
//reader reads the data and stores it in completeJsonData
reader.parse(file, completeJsonData);
//complete JSON data
cout << “Complete JSON data: “ << endl << completeJsGrade << endl;
//get the value associated with name key
cout << “Name: “ << completeJsonData[“name”] << endl;
//get the value associated with grade key
cout << “Grade: “ << completeJsonData[“grade”] << endl;
}
