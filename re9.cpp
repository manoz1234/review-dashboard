#include<iostream>
#include<iomanip>
#include<fstream>
#include<string>
#include<curl/curl.h>
#include<jsoncpp/json/json.h>
size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata)
{
	((std::string*)userdata)->append(ptr, size * nmemb);
	return size * nmemb;
}
int main(int argc, char* argv[])
{
	// GitLab API endpoint and access token
	//std::string gitlab_api_url = "https://gitlab.com/api/v4/projects/45421801/merge_requests/2/note";
	std::string gitlab_api_url = "https://gitlab.com/commentreviewdashboard/comment-display/-/merge_requests/2/diffs";
	std::string gitlab_access_token = "glpat-eH9hLd8QdyhXBjHdNKqf";
	// CSV file path
	std::string csv_file_path = "comments.csv";
	// Set up libcurl
	CURL* curl;
	CURLcode res;
	curl = curl_easy_init();
	if (curl)
	{
		// Set the GitLab API endpoint
		curl_easy_setopt(curl, CURLOPT_URL, gitlab_api_url.c_str());
		// Set the GitLab access token
		struct curl_slist *headerlist = NULL;
		std::string authorization = "Authorization: Bearer " + gitlab_access_token;
		headerlist = curl_slist_append(headerlist, authorization.c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);


		// Set the callback function for the response
		std::string response;
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
		// Perform the request
		res = curl_easy_perform(curl);
		if (res == CURLE_OK)
		{
			// Parse the JSON data
			Json::Value root;
			Json::Reader reader;
			bool parsingSuccessful = reader.parse(response, root);
			std::cout << "JSON data: " << std::endl;
			std::cout << response << std::endl;
			if (parsingSuccessful)
			{
				// Open the CSV file for writing
				std::ofstream csv_file(csv_file_path);
				// Write the CSV header row
				csv_file <<std::left <<std::setw(20) << "Developer Name"
                                         << std::left <<std::setw(30) << "Date"
                                         << std::left <<std::setw(20) << "Reviewer Name"
                                         << std::left << std::setw(20) << "file name"
                                         << std::left << std::setw(30) << "Review Comments" << std::endl; 
				// Loop through the discussions and write them to the CSV file
				for (int i = 0; i < root.size(); i++)
				{
					//std::cout<<"iteration - "<<i<<std::endl;
					std::string developer = root[i]/*["notes"]*/["author"]["name"].asString();
					std::string reviewer = root[i]/*["notes"][1] */["author"]["name"].asString();
					std::string date = root[i]["created_at"].asString();
					std::string file_path = root[i]/*["notes"][0][*/["position"]["new_path"].asString();
					std::string comment = root[i]/*["notes"][1]*/["body"].asString();
					csv_file << "\"" << developer << "\",\"" <<reviewer<<"\",\""<<date<<"\",\""<<file_path<<"\",\"" << comment  << "\"\n";
				}
				// Close the CSV file
				csv_file.close();
				std::cout << "comments exported to " << csv_file_path << std::endl;
			}
			else
			{
				std::cout << "Error parsing JSON data: " << reader.getFormattedErrorMessages() << std::endl;
			}
		}
		else
		{
			std::cout << "Error retrieving comments: " << curl_easy_strerror(res) << std::endl;
		}
		//Clean up libcurl
		curl_slist_free_all(headerlist);
		curl_easy_cleanup(curl);
	}
	return 0;
}
